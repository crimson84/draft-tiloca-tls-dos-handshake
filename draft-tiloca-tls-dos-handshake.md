---
title: Extension for protecting (D)TLS handshakes against Denial of Service
abbrev: (D)TLS extension against Denial of Service
docname: draft-tiloca-tls-dos-handshake-02
# date: 2017-04-25
category: std

ipr: trust200902
area: Security
workgroup: TLS Working Group
keyword: Internet-Draft

# stand_alone: yes

coding: us-ascii

#pi:    # can use array (if all yes) or hash here
pi: [toc, sortrefs, symrefs]
#  - toc
#  - sortrefs
#  - symrefs
#toc: yes
#sortrefs:   # defaults to yes
#symrefs: yes


author:
 -
    ins: M. Tiloca
    name: Marco Tiloca
    org: RISE SICS AB
    street: Isafjordsgatan 22
    city: Kista
    code: SE-164 29
    country: Sweden
    phone: +46 70 604 65 01
    email: marco.tiloca@ri.se

 -
    ins: L. Seitz
    name: Ludwig Seitz
    org: RISE SICS AB
    street: Scheelevaegen 17
    city: Lund
    code: SE-223 70
    country: Sweden
    phone: +46 70 349 92 51
    email: ludwig.seitz@ri.se

 -
    ins: M. Hoeve
    name: Maarten Hoeve
    org: ENCS
    street:  Regulusweg 5
    city: The Hague
    code: 2516 AC
    country: The Netherlands
    phone:  +31 62 015 75 51
    email: maarten.hoeve@encs.eu
    
 -
    ins: O. Bergmann
    name: Olaf Bergmann
    org: Universitaet Bremen TZI
    street:  Postfach 330440
    city: Bremen
    code: D-28359
    country: Germany
    phone:  +49 421 218 63904
    email: bergmann@tzi.org
        
normative:
  RFC2104:
  RFC2119:
  RFC5246:
  RFC6347:
  RFC8174:
  I-D.ietf-tls-tls13:
  I-D.ietf-tls-dtls13:

informative:
  RFC4366:
  I-D.ietf-core-resource-directory:

--- abstract

This document describes an extension for TLS and DTLS to protect the server from Denial of Service attacks against the handshake protocol, carried out by an on-path adversary. The extension includes a nonce and a Message Authentication Code (MAC) over that nonce, encoded as a Handshake Token that a Trust Anchor entity computes and provides to the client. The server registered at the Trust Anchor verifies the MAC to determine whether continuing or aborting the handshake.

--- middle

# Introduction {#sec-introduction}

Servers running TLS {{RFC5246}}{{I-D.ietf-tls-tls13}} and DTLS {{RFC6347}}{{I-D.ietf-tls-dtls13}} are vulnerable to Denial of Service (DoS) attacks during the very first step of the handshake protocol. That is, an adversary can repeatedly send ClientHello messages to the server and induce it to perform computations and execute handshakes, before stopping handshake executions and make the server hold state open.

DTLS 1.2 as well as both TLS 1.3 and DTLS 1.3 provide the optional Cookie exchange as possible solution to mitigate this DoS attack. This mechanism is specifically oriented towards adversaries that are not on-path. That is, the Cookie exchange makes the attack more complicated to mount. However, a well determined and resourceful on-path adversary, able to spoof valid IP addresses, can still successfully perform the DoS attack, by intercepting the possible server response including the Cookie and then echoing it in the second ClientHello. This is in particular possible if the handshake does not use Pre-Shared Key exchange modes.

More specifically, the handshake protocol is exposed to DoS attacks mounted by an on-path adversary, ranging minimally from a man-on-the-side (i.e. able to read and inject traffic, but not block) to maximally a full active adversary (i.e. able also to block traffic).

Depending on the specific protocol version and the key establishment mode used in the handshake, the attack impact can range from a single reply triggered by invalid ClientHello messages, to the server performing advanced handshake steps with consequent setup of invalid half-open sessions. Especially if performed in a large-scale and distributed manner, this attack can thwart performance and service availability of (D)TLS servers. Moreover, the attack can be particularly effective in application scenarios where servers are resource-constrained devices running DTLS over low-power, low bandwidth and lossy networks.

This specification describes a "dos_protection" extension for TLS and DTLS, included into ClientHello messages in order to mark them as valid and neutralize the DoS attacks mentioned above. In essence, the "dos_protection" extension includes a Handshake Token encoding a nonce and a Message Authentication Code (MAC) computed over that nonce. Upon receiving the ClientHello message, the server checks the MAC conveyed in the Handshake Token, and determines whether to either continue the handshake or to immediately abort it.

The proposed method relies on a Trust Anchor (TA) entity, which is in a trust relation with the server, and authorizes the client to establish a secure session with the server. In particular, the Trust Anchor computes the MAC encoded in the Handshake Token, before providing the latter to the client.

## Terminology {#ssec-terminology}

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 {{RFC2119}}{{RFC8174}} when, and only when, they appear in all capitals, as shown here.

Readers are expected to be familiar with terms and concepts related to TLS 1.2 {{RFC5246}} and DTLS 1.2 {{RFC6347}}, as well as to TLS 1.3 {{I-D.ietf-tls-tls13}} and DTLS 1.3 {{I-D.ietf-tls-dtls13}}, with particular reference to their respective handshake protocol.

This document refers also to the following terminology.

* Trust Anchor (TA): a trusted third party with a security association with the (D)TLS server. Compared to each single (D)TLS server it is associated to, the Trust Anchor is usually equipped with significant larger amounts of resources, especially in terms of computing power and memory availability.

* Master Key (K_M): a long-term symmetric key shared between the Trust Anchor and the server.

* Handshake Token (T): piece of information provided by the Trust Anchor to a client intending to start a handshake with the server. The Handshake Token is opaque to the client, i.e. the semantics of the Handshake Token are intelligible only to the Trust Anchor and the server.

* Nonce (N): an unsigned integer value used by the Trust Anchor to produce a fresh Handshake Token. The Trust Anchor maintains a pairwise counter separately for each associated server, in order to produce Nonce values.

# DoS Protection Extension {#sec-dos-protection-extension}

## Extension Type {#ssec-extension-type}

This specification extends the ExtensionType enum as follows:

~~~~~~~~~~
   enum {
       ...,
       dos_protection(TBD),
       (65535)
   } ExtensionType;
~~~~~~~~~~

## Extension Data {#ssec-extension-data}

The "extension_data" field of the "dos_protection" extension contains the following information:

~~~~~~~~~~
    struct {
      opaque handshake_token;
    } extension_data_content;
~~~~~~~~~~

The "handshake_token" field is intended to include the Handshake Token generated by the Trust Anchor. The Handshake Token encodes a nonce and a Message Authentication Code (MAC) computed over the nonce.

# Protocol overview {#sec-protocol-overview}

Before becoming fully operational, the server S registers at the TA through a secure communication channel or other out-of-band means. A server is registered at one TA only, while the same TA can be associated to multiple servers.

For each registered server S, the TA and S maintain a pairwise counter z_S, associated to that server and encoded as an unsigned integer. Upon S's registration, S and the TA initialize z_S to 0 and establish a long-term symmetric key K_M. The specific means to establish K_M are out of the scope of this specification.

The rest of this document refers to H as a hash function and to an HMAC {{RFC2104}} relying on H. The TA and the server MUST support the hash function SHA-256.
 
{{figprot}} shows the messages exchanged between the client (C), the Trust Anchor (TA) and the server (S).

~~~~~~~~~~
        C                                   TA                   S
        |                                   |                    |
        |                                   | { Shared key K_M } |
        |                                   |                    |
        | --- Request handshake with S ---> |                    |
  (1)   |                                   |                    |
        | <------- Handshake Token -------- |                    |
        |                                   |                    |
  ---   |                                   |                    |
        |                                                        |
        |      ClientHello with "dos_protection" extension       |
  (2)   | -----------------------------------------------------> |
        |             Including the Handshake Token              |
        |                                                        |
  ---   |                                   |                    |
        |                                   |                    |
        |                                                        |
  (3)   | [<-------------- Next handshake steps -------------->] |
        |                                                        |
        |                                   |                    |
~~~~~~~~~~
{: #figprot title="Protocol Overview"}

Step (1) concerns a client C that intends to start a (D)TLS session with the server S. That is, C contacts the TA and specifies its intention to start a (D)TLS handshake with S. The client C can rely on services such as {{I-D.ietf-core-resource-directory}} to know what is the specific TA associated to S. All communications between C and the TA must be secured, ensuring integrity, source authentication, confidentiality and replay protection of exchanged messages. The specific means to secure communications between C and the TA are out of the scope of this specification.

The TA must verify that C is authorized to establish a (D)TLS session with S. To this end, the TA can directly authorize the client, or expect the client to upload authorization evidence previously obtained from a trusted entity. Compared with models based on proxies, this approach does not require particular adaptations to the communication between clients and servers. The specific authorization process of clients is out of the scope of this specification.

In case of successful authorization, the TA provides C with a fresh Handshake Token, which encodes a nonce as well as a Message Authentication Code (MAC) computed over the nonce using the key K_M. The Handshake Token is opaque to the client. Besides, the client must consume this Handshake Token right away, and in particular before asking the TA for a new Handshake Token intended for the same server S.

During Step (2), C prepares the ClientHello message addressed to S, including the "dos_protection" extension defined in {{sec-dos-protection-extension}}. In particular, the extension includes the Handshake Token received by the TA, as content of the field "handshake_token". Then, C sends the ClientHello message to S. The overall content and format of the ClientHello message depend on the specific version of (D)TLS.

Upon receiving the ClientHello message, the server S retrieves the Handshake Token from the "dos_protection" extension. Then, S relies on the nonce included in the Handshake Token to check that the ClientHello message is not a replay. After that, S uses the key K_M to recompute the MAC, and checks it against the MAC encoded in the received Handshake Token.

In case the ClientHello message is fresh and the MAC is valid, S continues to Step (3), i.e., it proceeds with the handshake with C. Otherwise, S discards the ClientHello message and aborts the handshake.

# Client to Trust Anchor {#sec-client-to-trust-anchor}

The client C requests from the TA an authorization to open a new (D)TLS session with the server S. That is, this step does not take place if C intends to resume a (D)TLS session previously established with S. Considerations about session resumption are provided in {{sec-session-resumption}}.

In case of successful authorization, the TA selects the nonce N as the current value of the pairwise counter z_S associated to S. Then, the TA performs the following actions.

1. It sets the variable token_nonce to the nonce N.

2. It computes a MAC as the output of HMAC(K_M, H(token_nonce)).

3. It builds a Handshake Token including token_nonce and the MAC.

After that, the TA provides the Handshake Token to C, and increments the counter z_S by 1.

The TA handles a wrap-around of the counter z_S by renewing the Master Key K_M as described in {{ssec-security-considerations-key_renewal}}.

# Client to Server {#sec-client-to-server}

This section considers a client C intending to establish a new (D)TLS session with S. Considerations about session resumption are provided in {{sec-session-resumption}}.

Once it has received the Handshake Token from the TA, the client C must consume it right away, by including it in a ClientHello message addressed to the server S. In particular, the client C must consume this Handshake Token before asking the TA for a new one intended for the same server S. The client C considers the Handshake Token consumed, and hence discards it, once received a valid ServerHello message during the same handshake with the server S.

Furthermore, the client discards a Handshake Token also in case of handshake abortion due to too many retransmissions of a same ClientHello message. In such a case, the client must ask the TA for a new, i.e. fresh, Handshake Token and start over a new handshake with the server S.

When preparing the ClientHello message, the client C proceeds as follows.

1. It builds the "dos_protection" extension defined in {{sec-dos-protection-extension}}.

2. It includes the Handshake Token received from the TA in the "handshake_token" field of the "dos_protection" extension.

3. It includes the "dos_protection" extension into the ClientHello message, consistently with what is mandated and recommended by the specific version of (D)TLS.

Once the ClientHello message has been completely prepared, C transmits it to S. Note that C retransmits exactly the same "dos_protection" extension from this first ClientHello message, in case it sends a second ClientHello message as a reply to a HelloVerifyRequest in DTLS 1.2 or a HelloRetryRequest in (D)TLS 1.3.

# Server Processing {#sec-server-processing}

This section considers a server S receiving a ClientHello message from C for initiating a new (D)TLS session. Considerations on session resumption are provided in {{sec-session-resumption}}.

A server MAY require clients to send a valid "dos_protection" extension. A server requiring this MUST respond to a ClientHello lacking a "dos_protection" extension by terminating the handshake, with a "missing_extension" alert if the client has shown support for (D)TLS 1.3, or a "handshake_failure" alert otherwise.

Upon receiving the first ClientHello message from C, the server S retrieves the Handshake Token from the "handshake_token" field of the "dos_protection" extension.

Then, the server S MUST check that the ClientHello message is not a replay. {{sec-replay-protection}} of this specification describes a possible method to perform the anti-replay check, based on the nonce encoded in the Handhshake Token. If the ClientHello message is found to be not fresh, then S discards it and terminates the handshake with a "handshake_failure" alert.

If the ClientHello message is found to be fresh, then S performs the following actions.

1. It retrieves token_nonce from the Handshake Token.

2. It computes a MAC as the output of HMAC(K_M, H(token_nonce)).

If the computed MAC differs from the MAC encoded in the Handshake Token, S discards the ClientHello message and terminates the handshake with a "handshake_failure" alert. Otherwise, S continues performing the handshake with C.

# Replay Protection {#sec-replay-protection}

This section describes a possible method to perform anti-replay checks on received ClientHello messages, based on the nonce encoded in the Handshake Token as token_nonce.

The server S maintains a sliding window W of size A, as a pair {w, w_b}, where w is an A-bit vector and w_b indicates the current left bound of W. That is, w_b indicates the lowest value that S can accept as the nonce N encoded in the Handshake Token as token_nonce. Upon startup, S sets w_b to 0 and all bits in w to 0.

Upon receiving a ClientHello message for establishing a new (D)TLS session, the server S considers the nonce N encoded in the Handshake Token as token_nonce, and performs the following checks. As an example, the following considers a 32-bit nonce N.

* If N < w_b, then S discards the ClientHello message and terminates the handshake.

* If w_b <= N < min(w_b + A, 2^32), then S defines i = (N - w_b), and checks the i-th bit of vector w. If such bit is set to 1, i.e. the same nonce N has been already used, then S discards the ClientHello message and terminates the handshake. Instead, if such bit is set to 0, then S proceeds with processing the "dos_protection" extension as described in {{sec-server-processing}}.

* If (w_b + A) <= N < 2^32, then S proceeds with processing the "dos_protection" extension as described in {{sec-server-processing}}.

During this handshake execution, S discards any possible first ClientHello message including the same nonce N encoded in the Handshake Token as token_nonce.

Once the handshake has been successfully completed, S checks whether the condition N >= w_b is still valid. In such a case, S updates the window W as follows.

* If w_b <= N < min(w_b + A, 2^32), then S defines i = (N - w_b) and sets the i-th bit of vector w to 1, so marking N as used. Instead,

* if (w_b + A) <= N < 2^32, then S defines w* = (N - A + 1) and updates vector w as w = w >> (w* - w_b), where '>>' is the unsigned right bit shift operator. After that, S updates w_b as w_b = w*. Finally, S defines i = (N - w_b) and sets the i-th bit of vector w to 1, so marking N as used.

The window size A should be determined based on the expected frequency of new session establishments on the server S. Evidently, the larger the window, the more accurate is the replay protection, but the greater the memory overhead on the server side.

Furthermore, the window size A should take into account the time required for a client to request and get a Handshake Token from the TA, as well as to to deliver it to the (D)TLS server in the ClientHello message. This is necessary in order to avoid that the sliding window advances too fast, and hence that the (D)TLS server discards such ClientHello messages as stale.

# Session Resumption {#sec-session-resumption}

In case a client C sends a ClientHello message asking to resume a session, the server S relies on the existing association with C and hence does not need a further assertion of client's validity from the TA. In addition, S can rely on the Client Hello Recording mechanism described in Section 8 of {{I-D.ietf-tls-tls13}}, in order to perform anti-replay checks on ClientHello messages asking for session resumption.

As a consequence, the "dos_protection" extension defined in {{sec-dos-protection-extension}} is not strictly necessary in ClientHello messages sent for session resumption.

However, Section 7.4.1.4 of {{RFC5246}} states that a client asking for session resumption SHOULD send the same extensions as it would if it was not attempting resumption. At the same time, it states that most extensions are relevant only when a new session is initiated, and hence the server would not process them in case of session resumption.

In accordance with such guidelines, a server S can possibly instruct the TA to also provide requesting clients with a small number R of additional Resumption Tokens.

In order to compute each of the Resumption Tokens for a same request from a given client, the TA MUST use the same nonce value N used to compute the Handshake Token (see {{sec-client-to-trust-anchor}}). In particular, the TA computes the i-th Resumption Token, 0 <= i < R, as follows.

1. It sets the variable token_nonce to (N + i), where '+' is the concatenate operator.

3. It computes a MAC as the output of HMAC(K_M, H(token_nonce)).

4. It builds the i-th Resumption Token including token_nonce and the MAC.

Finally, the TA provides the requesting client with the Handshake Token and the additional Resumption Tokens. The client MUST use the Handshake Token during a handshake with S for session initiation, as described in {{sec-client-to-server}}. The client MUST use the i-th Resumption Token upon attempting the i-th resumption of that session. After it has used all the Resumption Tokens received from the TA, the client must assume that S does not support further resumptions of the same session.

Upon receiving a ClientHello message from C asking to resume a session, the server S verifies the MAC encoded in the Resumption Token as described in {{sec-server-processing}}. However, S does not rely on the "dos_protection" extension and the token_nonce in the Resumption Token to perform an anti-replay check.

Further details about session resumption are defined in the (D)TLS specifications of the different respective versions.

# Security Considerations {#sec-security-considerations}

This specification does not change the intended security properties of TLS and DTLS. Additional security aspects are discussed below.

## Security Effectiveness {#ssec-security-considerations-effectiveness}

The MAC encoded in the "dos_protection" extension as part of the Handshake Token is computed only over the 'token_nonce' part of the same Handshake Token. That is, a server S can actually assert the validity and freshness of the Handshake Token only, rather than of the whole ClientHello message.

As a consequence, an on-path adversary can intercept ClientHello messages sent by legitimate clients, retrieve the "dos_protection" extension, and then use it inside forged ClientHello messages injected and addressed to the server. However, this practically displays negligible consequences in terms of additional impact on the server, as discussed in the following.

On one hand, a man-on-the-side adversary, namely able to intercept and inject traffic but not block, can, with reasonable effort, exploit the limitation above in order to induce the server to negotiate more expensive cipher suites, which is fair to consider as a weak attack achievement. Furthermore, the injection of such forged ClientHello messages including a stolen "dos_protection" extension is anyway rate limited by the number of legitimate clients and the frequency of their handshake executions.

On the other hand, a full active adversary, namely able to also block traffic, would not even bother to inject forged ClientHello messages including a stolen "dos_protection" extension. In fact, (s)he can more easily let the server process handshake messages from legitimate clients during handhshake early phases, and later on block specific client messages during handshake advanced phases, so leaving the server with several half-open sessions and open states. Again, this is anyway rate limited by the number of legitimate clients and the frequency of their handshake executions.

## Trust Anchor as Target {#ssec-security-considerations-target_TA}

Communications between clients and the TA may be secured by means of (D)TLS, with the TA acting as server. In such a case, the TA becomes also a target for the DoS attack addressed in this specification.

On the other hand, TAs are expected to be equipped with plentiful of resources, i.e. in significant larger amounts than each of the associated (D)TLS servers. That is, given a class of adversary targeting a number of (D)TLS servers, the corresponding TA is practically not a feasible target for that adversary.

Besides, while it is infeasible to expect a considerably high number of (resource-contrained) (D)TLS servers to be robust against DoS by construction, it is instead feasible to have relatively few deployed TAs which are able to endure this attack when carried out against them. This might in turn encourage an adversary to rather target a TA, in order to indirectly make the (D)TLS servers unavailable to serve clients. However, as discussed above, a class of adversary targeting a (D)TLS server is not supposed to have sufficient resources to effectively compromise the availability of the corresponding TA.

Furthermore, a typical starting point for an adversary consists in identifying the set of victim servers, as belonging to the same application/administrative domain(s) or network segment(s). Hence, the adversary would be motivated in targeting the TA(s) associated to the (D)TLS servers in those segments. As an additional deterrent, (D)TLS servers in a same segment or domain can thus be registered at different TAs, in order to further reduce the feasibility and spread the effectiveness of attacks rather addressed against those TAs.

## Renewal of Long-Term Key K_M {#ssec-security-considerations-key_renewal}

While it can practically take a long amount of time, the pairwise counter z_S maintained by the TA and associated to S eventually wraps around. When this happens, the TA MUST revoke the key K_M shared with S, in order to not reuse {K_M, N} pairs when building Handshake Tokens for requesting clients.

In particular, when the counter z_S wraps-around, the TA MUST perform the following actions.

1. It stops accepting requests related to S from clients.

2. It securely generates a new long-term key K_M and securely provides it to S.

3. It resumes serving requests related to S from clients, using the new K_M to compute MACs when building Handshake Tokens.

## Rate Limit to Nonce Release {#ssec-security-considerations-release-nonces}

It is RECOMMENDED that the TA does not release Handshake Tokens to clients beyond a maximum rate. This prevents a client with legitimate credentials from quickly consuming the nonce space associated to S, and thus making the TA unable to serve other clients.

# IANA Considerations {#sec-iana}

IANA is requested to allocate an entry to the existing TLS "ExtensionType" registry defined in {{RFC5246}} and originally created in {{RFC4366}}, for dos_protection (TBD) defined in this document.

# Acknowledgments {#sec-acknowledgments}

The authors are sincerely thankful to Santiago Arag&oacute;n, Rolf Blom and Eric Rescorla for their comments and feedback.

The work on this document has been partly supported by the EU FP7 project SEGRID (Grant Agreement no. 607109) and the EIT-Digital High Impact Initiative ACTIVE.

--- back
